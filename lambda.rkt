#lang sicp

;; 1.3.2 Constructing Procedures Using lambda

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (cube x) (* x x x))

(define square (lambda (x) (* x x)))



(define(pi-sum a b)
  (sum (lambda (x) (/ 1.0 (* x (+ x 2))))
       a
       (lambda (x) (+ x 4))
       b))

(* (pi-sum 1 10000) 8)

(define (integral f a b dx)
  (* (sum f
          (+ a (/ dx 2.0))
          (lambda (x) (+ x dx))
          b)
     dx))

(integral cube 0 1 0.001)

((lambda (x y z) (+ x y (square z)))
 1 2 3)

; Using let to create local variables

(define (f1 x y)
  (define (f-helper a b)
    (+ (* x (square a))
       (* y b)
       (* a b)))
  (f-helper (+ 1 (* x y))
            (- 1 y)))

(f1 2 5)

(define (f2 x y)
  ((lambda (a b)
    (+ (* x (square a))
       (* y b)
       (* a b)))
   (+ 1 (* x y))
            (- 1 y)))

(f2 2 5)

(define (f3 x y)
  (let ((a (+ 1 (* x y)))
        (b (- 1 y)))
    (+ (* x (square a))
       (* y b)
       (* a b))))

(f3 2 5)

; example
(define x 5)
(+ (let ((x 3))
     (+ x (* x 10)))
   x)

(define (f4 x y)
  (define a (+ 1 (* x y)))
  (define b (- 1 y))
   (+ (* x (square a))
       (* y b)
       (* a b)))

(f4 2 5)