#lang sicp

;; 1.2.6 Example: Testing for Primality

(define (square x) (* x x))

;; Exercise 1.23
(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (next divisor)
  (if (= divisor 2)
      3
      (+ divisor 2)))

(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder
          (square (expmod base (/ exp 2) m))
          m))
        (else
         (remainder
          (* base (expmod base (- exp 1) m))
          m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

;; Exercise 1.21
(display "smallest-divisor 199 = ")
(smallest-divisor 199)
(newline)
(display "smallest-divisor 1999 = ")
(smallest-divisor 1909)
(newline)
(display "smallest-divisor 19999 = ")
(smallest-divisor 19999)
(newline)

;; Exercise 1.24
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (fast-prime? n 10)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-primes a b)
  (cond ((> a b)
             (newline) (display "done")(newline))
            (else (timed-prime-test a)
                  (search-for-primes (+ 1 a) b))))



; 1009 *** 3
; 1013 *** 4
; 1019 *** 5

; 10007 *** 9
; 10009 *** 9
; 10037 *** 9

; 100003 *** 24
; 100019 *** 23
; 100043 *** 22

; 1000003 *** 68
; 1000033 *** 68
; 1000037 *** 69

;; Exercise 1.25

(define (fast-expt b n)
 ; (display "calling ")(display b)(display " ")(display n)(newline)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (expmod2 base exp m)
  (remainder (fast-expt base exp) m))

 (search-for-primes 1000 1500)

(define (carmichael-test n)
  (test-iter n 1 true))

(define (test-iter n a so-far)
  ;(display "calling test-iter ")(display n)(display " ")(display a)(display " ")(display so-far)(newline)
  (cond ((equal? so-far false) false)
        ((= a n) so-far)
        (else (test-iter n (+ a 1) (= (expmod a n n) (remainder a n))))))

(carmichael-test 561)
(smallest-divisor 561)
(carmichael-test 1105)
(smallest-divisor 1105)
(carmichael-test 1729)
(smallest-divisor 1729)
(carmichael-test 2465)
(smallest-divisor 2465)
(carmichael-test 2821)
(smallest-divisor 2821)
(carmichael-test 6601)
(smallest-divisor 6601)