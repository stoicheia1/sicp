#lang sicp

(define (cube x) (* x x x))

;(define (sum-integers a b)
;  (if (> a b)
;      0
;      (+ a (sum-integers (+ a 1) b))))

;(define (sum-cubes a b)
;  (if (> a b)
;      0
;      (+ (cube a)
;         (sum-cubes (+ a 1) b))))

(define pi 3.14159)

;(define (pi-sum a b)
;  (if (> a b)
;      0
;      (+ (/ 1.0 (* a (+ a 2)))
;         (pi-sum (+ a 4) b))))

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (inc n) (+ n 1))
(define (sum-cubes a b)
  (sum cube a inc b))

;(sum-cubes 1 10)

(define (identity x) x)
(define (sum-integers a b)
  (sum identity a inc b))

(define (pi-sum a b)
  (define (pi-term x)
    (/ 1.0 (* x (+ x 2))))
  (define (pi-next x)
    (+ x 4))
  (sum pi-term a pi-next b))

;(* 8 (pi-sum 1 10000000))
(* 8 (pi-sum 1 1000))

(define (integral f a b dx)
  (define (add-dx x)
    (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

; Exercise 1.29
(define (integral-simpson f a b n)
  (define h (/ (- b a) n))
  (define (simpson-term k)
    (* (multiple k n) (f (+ a (* k h)))))
  (* (/ h 3.0)
     (sum simpson-term 0 inc n)))

(define (multiple x n)
    (cond ((or (= x 0) (= n x)) 1)
          ((= 0 (modulo x 2)) 2)
          (else 4)))


(multiple 0 100)
(multiple 1 100)
(multiple 2 100)
(multiple 100 100)

(integral cube 0 1 0.01)
(integral cube 0 1 0.001)
(integral cube 0 1 0.0001)
;(integral cube 0 1 0.000001)

(integral-simpson cube 0 1 100)
(integral-simpson cube 0 1 1000)